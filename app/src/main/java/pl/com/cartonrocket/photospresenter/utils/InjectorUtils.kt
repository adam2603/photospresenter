package pl.com.cartonrocket.photospresenter.utils

import pl.com.cartonrocket.photospresenter.models.ImagesModel
import pl.com.cartonrocket.photospresenter.viewmodels.GalleryViewModelFactory

object InjectorUtils {

    private val imagesModel get() = ImagesModel()

    fun provideGalleryViewModelFactory() = GalleryViewModelFactory(this::imagesModel)

}