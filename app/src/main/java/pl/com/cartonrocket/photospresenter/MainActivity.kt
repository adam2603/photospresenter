package pl.com.cartonrocket.photospresenter

import android.app.ActivityManager
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts.PickMultipleVisualMedia
import androidx.activity.result.contract.ActivityResultContracts.PickVisualMedia
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import pl.com.cartonrocket.photospresenter.adapters.ImagesAdapter
import pl.com.cartonrocket.photospresenter.databinding.ActivityMainBinding
import pl.com.cartonrocket.photospresenter.fragments.PreviewFragment
import pl.com.cartonrocket.photospresenter.utils.InjectorUtils
import pl.com.cartonrocket.photospresenter.utils.ViewAnimator
import pl.com.cartonrocket.photospresenter.viewmodels.GalleryViewModel

class MainActivity : AppCompatActivity() {

    private val imagesAdapter = ImagesAdapter()
    private lateinit var viewModel: GalleryViewModel
    private lateinit var viewAnimator: ViewAnimator
    private lateinit var binding: ActivityMainBinding
    private val getContent = registerForActivityResult(PickMultipleVisualMedia()) {
        handleImageSelectResult(it)
    }
    private val onBackPressedCallback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            viewModel.onImageDeselected()
        }
    }

    private var isPreviewOpen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        onBackPressedDispatcher.addCallback(onBackPressedCallback)

        val factory = InjectorUtils.provideGalleryViewModelFactory()
        viewModel = ViewModelProvider(this, factory)[GalleryViewModel::class.java]

        viewModel.activeImage.observe(this) {
            onBackPressedCallback.isEnabled = it.second != null
        }

        binding.fab.setOnClickListener {
            if (binding.fab.removeActionActive) viewModel.onDeleteSelectedImages()
            else openImageChooser()
        }

        val shortAnimationDuration =
            resources.getInteger(android.R.integer.config_shortAnimTime).toLong()
        viewAnimator = ViewAnimator(shortAnimationDuration)

        imagesAdapter.onImageClicked = viewModel::onImageSelected
        imagesAdapter.onImageLongClick = {
            viewModel.onImageLongClicked(it)
            true
        }

        binding.mainContent.recyclerGallery.apply {
            layoutManager = GridLayoutManager(
                this@MainActivity,
                resources.getInteger(R.integer.photosInRow)
            )
            adapter = imagesAdapter
        }

        viewModel.images.observe(this, Observer {
            binding.mainContent.hintGroup.visibility =
                if (it.isNullOrEmpty()) View.VISIBLE else View.GONE
            imagesAdapter.imagesSrc = it ?: return@Observer
        })
        viewModel.selectedImages.observe(this, Observer {
            imagesAdapter.selectedImages = it ?: return@Observer
        })

        viewModel.activeImage.observe(this, Observer {
            val (oldIndex, newIndex) = it ?: return@Observer
            if (isPreviewOpen && newIndex == null) closePreview(oldIndex ?: 0)
            else if (!isPreviewOpen && newIndex != null) openPreview(newIndex)
        })

        viewModel.isAppLocked.observe(this) {
            if (it == true) binding.fab.hide()
            else binding.fab.show()
        }

        viewModel.isSelectingModeEnabled.observe(this) {
            binding.fab.removeActionActive = it ?: false
        }
    }

    private fun openImageChooser() {
        getContent.launch(PickVisualMediaRequest(PickVisualMedia.ImageOnly))
    }

    private fun openPreview(position: Int) {
        isPreviewOpen = true
        val thumbnailView =
            binding.mainContent.recyclerGallery.findViewHolderForAdapterPosition(position)?.itemView
        getPreviewFragment().loadImageAt(position) {
            if (thumbnailView != null) {
                viewAnimator.animateTransition(
                    thumbnailView,
                    binding.previewFragmentContainer,
                    binding.mainContainer
                )
            } else binding.previewFragmentContainer.visibility = View.VISIBLE
        }
    }

    private fun getPreviewFragment() =
        (supportFragmentManager.findFragmentById(R.id.previewFragment) as PreviewFragment)

    private fun closePreview(position: Int) {
        isPreviewOpen = false
        binding.mainContent.recyclerGallery.scrollToPosition(position)
        val thumbnailView =
            binding.mainContent.recyclerGallery.findViewHolderForAdapterPosition(position)?.itemView
        if (thumbnailView != null) {
            viewAnimator.reverseAnimateTransition(
                thumbnailView,
                binding.previewFragmentContainer,
                binding.mainContainer
            ) {
                getPreviewFragment().loadImageAt(null)
            }
        } else {
            viewAnimator.animateHide(binding.previewFragmentContainer)
            getPreviewFragment().loadImageAt(null)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.actionLock -> {
                startLockTask()
                viewModel.onAppLocked()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val isLocked = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activityManager.lockTaskModeState != ActivityManager.LOCK_TASK_MODE_NONE
        } else {
            activityManager.isInLockTaskMode
        }

        if (!isLocked) viewModel.onAppUnlocked()
    }

    private fun handleImageSelectResult(uris: List<Uri>?) {
        if (uris != null) {
            viewModel.onImagesAdded(uris)
        }
    }
}
