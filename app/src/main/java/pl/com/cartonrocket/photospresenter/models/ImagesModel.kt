package pl.com.cartonrocket.photospresenter.models

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map

class ImagesModel {

    private val mImages = MutableLiveData<MutableList<Uri>>().apply { value = mutableListOf() }

    val images: LiveData<List<Uri>> = mImages.map { it.toList() }

    fun onImageAdded(imageSrc: Uri) {
        mImages.value = mImages.value?.apply { add(imageSrc) }
    }

    fun onImagesAdded(imagesSrc: List<Uri>) {
        mImages.value = mImages.value?.apply { addAll(imagesSrc) }
    }

    fun removeImagesAt(indexes: List<Int>) {
        val images = mImages.value
        indexes.sortedDescending().forEach { images?.removeAt(it) }
        mImages.value = images
    }

}