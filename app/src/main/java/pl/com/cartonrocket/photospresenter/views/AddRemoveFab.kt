package pl.com.cartonrocket.photospresenter.views

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.floatingactionbutton.FloatingActionButton
import pl.com.cartonrocket.photospresenter.R

class AddRemoveFab : FloatingActionButton {

    companion object {
        private val STATE_ACTION_REMOVE = intArrayOf(R.attr.state_action_remove)
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var removeActionActive = false
        set(value) {
            field = value
            refreshDrawableState()
        }

    override fun onCreateDrawableState(extraSpace: Int): IntArray {
        return if(removeActionActive) {
            val drawableState = super.onCreateDrawableState(extraSpace + 1)
            mergeDrawableStates(drawableState, STATE_ACTION_REMOVE)
            drawableState
        } else {
            super.onCreateDrawableState(extraSpace)
        }
    }
}