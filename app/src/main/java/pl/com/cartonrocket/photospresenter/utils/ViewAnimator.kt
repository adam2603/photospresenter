package pl.com.cartonrocket.photospresenter.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.graphics.Point
import android.graphics.Rect
import android.graphics.RectF
import android.view.View
import android.view.animation.DecelerateInterpolator

class ViewAnimator(private val shortAnimationDuration: Long) {

    private var currentAnimator: Animator? = null

    fun animateTransition(
        sourceView: View,
        targetView: View,
        viewsContainer: View,
        onAnimationEnd: () -> Unit = {}
    ) {
        val (startBounds, finalBounds) = getContainerRelativeStartEndRects(
            sourceView,
            viewsContainer
        )

        val startScale = transformStartRectToFinalRectRatio(startBounds, finalBounds)

        sourceView.alpha = 0f
        targetView.visibility = View.VISIBLE
        targetView.alpha = 1f

        targetView.pivotX = 0f
        targetView.pivotY = 0f

        currentAnimator = AnimatorSet().apply {
            playTogether(
                ObjectAnimator.ofFloat(targetView, View.X, startBounds.left, finalBounds.left),
                ObjectAnimator.ofFloat(targetView, View.Y, startBounds.top, finalBounds.top),
                ObjectAnimator.ofFloat(targetView, View.SCALE_X, startScale, 1f),
                ObjectAnimator.ofFloat(targetView, View.SCALE_Y, startScale, 1f)
            )
            duration = shortAnimationDuration
            interpolator = DecelerateInterpolator()
            addListener(AnimatorEndListener {
                currentAnimator = null
                sourceView.alpha = 1f
                onAnimationEnd.invoke()
            })
            start()
        }
    }

    fun reverseAnimateTransition(
        sourceView: View,
        targetView: View,
        viewsContainer: View,
        onAnimationEnd: () -> Unit = {}
    ) {
        val (startBounds, finalBounds) = getContainerRelativeStartEndRects(
            sourceView,
            viewsContainer
        )

        val startScale = transformStartRectToFinalRectRatio(startBounds, finalBounds)

        sourceView.alpha = 0f
        targetView.visibility = View.VISIBLE
        targetView.alpha = 1f

        targetView.pivotX = 0f
        targetView.pivotY = 0f

        currentAnimator?.cancel()
        currentAnimator = AnimatorSet().apply {
            playTogether(
                ObjectAnimator.ofFloat(targetView, View.X, startBounds.left),
                ObjectAnimator.ofFloat(targetView, View.Y, startBounds.top),
                ObjectAnimator.ofFloat(targetView, View.SCALE_X, startScale),
                ObjectAnimator.ofFloat(targetView, View.SCALE_Y, startScale)
            )
            duration = shortAnimationDuration
            interpolator = DecelerateInterpolator()
            addListener(AnimatorEndListener {
                currentAnimator = null
                sourceView.alpha = 1f
                targetView.visibility = View.INVISIBLE
                onAnimationEnd.invoke()
            })
            start()
        }
    }

    private fun getContainerRelativeStartEndRects(
        sourceView: View,
        viewsContainer: View
    ): Pair<RectF, RectF> {
        val startBoundsInt = Rect()
        val finalBoundsInt = Rect()
        val globalOffset = Point()

        sourceView.getGlobalVisibleRect(startBoundsInt)
        viewsContainer.getGlobalVisibleRect(finalBoundsInt, globalOffset)

        startBoundsInt.offset(-globalOffset.x, -globalOffset.y)
        finalBoundsInt.offset(-globalOffset.x, -globalOffset.y)

        val startBounds = RectF(startBoundsInt)
        val finalBounds = RectF(finalBoundsInt)
        return Pair(startBounds, finalBounds)
    }

    private fun transformStartRectToFinalRectRatio(startBounds: RectF, finalBounds: RectF): Float {
        val startScale: Float
        if ((finalBounds.width() / finalBounds.height() > startBounds.width() / startBounds.height())) {
            // Extend start bounds horizontally
            startScale = startBounds.height() / finalBounds.height()
            val startWidth: Float = startScale * finalBounds.width()
            val deltaWidth: Float = (startWidth - startBounds.width()) / 2
            startBounds.left -= deltaWidth.toInt()
            startBounds.right += deltaWidth.toInt()
        } else {
            // Extend start bounds vertically
            startScale = startBounds.width() / finalBounds.width()
            val startHeight: Float = startScale * finalBounds.height()
            val deltaHeight: Float = (startHeight - startBounds.height()) / 2
            startBounds.top -= deltaHeight.toInt()
            startBounds.bottom += deltaHeight.toInt()
        }
        return startScale
    }

    fun animateHide(view: View) {
        view.animate().alpha(0f).setListener(
            AnimatorEndListener {
                view.visibility = View.INVISIBLE
            }
        )
    }

    class AnimatorEndListener(private val onEnd: () -> Unit) : AnimatorListenerAdapter() {

        override fun onAnimationEnd(animation: Animator) {
            onEnd.invoke()
        }

        override fun onAnimationCancel(animation: Animator) {
            onEnd.invoke()
        }
    }

}