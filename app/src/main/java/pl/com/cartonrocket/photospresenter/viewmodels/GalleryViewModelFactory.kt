package pl.com.cartonrocket.photospresenter.viewmodels


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import pl.com.cartonrocket.photospresenter.models.ImagesModel

class GalleryViewModelFactory(private val newImagesModel: () -> ImagesModel) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return GalleryViewModel(newImagesModel.invoke()) as T
    }
}