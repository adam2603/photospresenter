package pl.com.cartonrocket.photospresenter.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import pl.com.cartonrocket.photospresenter.R

class ImagesAdapter : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    var imagesSrc = listOf<Uri>()
        set(value) {
            val diffs = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldIndex: Int, newIndex: Int) = (field[oldIndex] == value[newIndex])
                override fun areContentsTheSame(oldIndex: Int, newIndex: Int) = (field[oldIndex] == value[newIndex])
                override fun getOldListSize() = field.size
                override fun getNewListSize() = value.size
            })
            field = value
            diffs.dispatchUpdatesTo(this)
        }

    var selectedImages = listOf<Int>()
        set(value) {
            val diffs = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                private val before = List(imagesSrc.size) { field.contains(it) }
                private val after = List(imagesSrc.size) { value.contains(it) }

                override fun areItemsTheSame(oldIndex: Int, newIndex: Int) = (oldIndex == newIndex)
                override fun areContentsTheSame(oldIndex: Int, newIndex: Int) = (before[oldIndex] == after[newIndex])
                override fun getOldListSize() = after.size
                override fun getNewListSize() = after.size
            })
            field = value
            diffs.dispatchUpdatesTo(this)
        }

    var onImageClicked: (Int) -> Unit = {}
    var onImageLongClick: (Int) -> Boolean = { false }

    override fun onCreateViewHolder(container: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(container.context).inflate(R.layout.item_image, container, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = imagesSrc.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.setOnClickListener { onImageClicked.invoke(viewHolder.adapterPosition) }
        viewHolder.itemView.setOnLongClickListener { onImageLongClick.invoke(viewHolder.adapterPosition) }
        viewHolder.selectedIndicatorView.visibility = if (selectedImages.contains(viewHolder.adapterPosition)) View.VISIBLE else View.GONE
        Glide.with(viewHolder.imageView).load(imagesSrc[position]).centerCrop().into(viewHolder.imageView)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        Glide.with(holder.imageView.context).clear(holder.imageView)
        super.onViewRecycled(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.findViewById<ImageView>(R.id.mainImageView)!!
        val selectedIndicatorView = view.findViewById<View>(R.id.selectedIndicatorImageView)!!
    }

}