package pl.com.cartonrocket.photospresenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import pl.com.cartonrocket.photospresenter.adapters.ImagesPagerAdapter
import pl.com.cartonrocket.photospresenter.databinding.FragmentPresentBinding
import pl.com.cartonrocket.photospresenter.utils.InjectorUtils
import pl.com.cartonrocket.photospresenter.viewmodels.GalleryViewModel

class PreviewFragment : Fragment() {

    private lateinit var viewModel: GalleryViewModel

    private var binding: FragmentPresentBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        FragmentPresentBinding.inflate(inflater, container, false)
            .run {
                binding = this
                root
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = InjectorUtils.provideGalleryViewModelFactory()
        viewModel = ViewModelProvider(requireActivity(), factory)[GalleryViewModel::class.java]

        binding?.imagesPager?.addOnPageChangeListener(
            object : ViewPager.SimpleOnPageChangeListener() {
                override fun onPageSelected(position: Int) {
                    viewModel.onImageSelected(position)
                }
            }
        )
    }

    fun loadImageAt(position: Int?, onImageReady: () -> Unit = {}) {
        if (position != null) {
            var callbackFired = false
            binding?.imagesPager?.adapter =
                ImagesPagerAdapter(viewModel.images.value ?: emptyList()) {
                    if (!callbackFired && it == position) {
                        callbackFired = true
                        onImageReady.invoke()
                    }
                }
            binding?.imagesPager?.currentItem = position
        } else {
            binding?.imagesPager?.adapter = null
            onImageReady.invoke()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}