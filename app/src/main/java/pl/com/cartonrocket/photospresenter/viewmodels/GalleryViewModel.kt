package pl.com.cartonrocket.photospresenter.viewmodels

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import pl.com.cartonrocket.photospresenter.models.ImagesModel

class GalleryViewModel(private val imagesModel: ImagesModel) : ViewModel() {

    private val mActiveImage = MutableLiveData<Pair<Int?, Int?>>()
    private val mIsAppLocked = MutableLiveData<Boolean>()
    private val mIsSelectingModeEnabled = MediatorLiveData<Boolean>()
    private val mSelectedImages = MutableLiveData<List<Int>>()

    init {
        mSelectedImages.value = emptyList()
        mSelectedImages.observeForever { mIsSelectingModeEnabled.value = it.isNotEmpty() }
    }

    val images = imagesModel.images
    val activeImage = mActiveImage as LiveData<Pair<Int?, Int?>>
    val isAppLocked = mIsAppLocked as LiveData<Boolean>
    val selectedImages = mSelectedImages as LiveData<List<Int>>
    val isSelectingModeEnabled = mIsSelectingModeEnabled as LiveData<Boolean>

    fun onImageAdded(imageSrc: Uri) {
        imagesModel.onImageAdded(imageSrc)
    }

    fun onImagesAdded(imagesSrc: List<Uri>) {
        imagesModel.onImagesAdded(imagesSrc)
    }

    fun onImageSelected(index: Int) {
        if(mIsSelectingModeEnabled.value == true) {
            onImageLongClicked(index)
        } else {
            mActiveImage.value = Pair(mActiveImage.value?.second, index)
        }
    }

    fun onImageDeselected() {
        mActiveImage.value = Pair(mActiveImage.value?.second, null)
    }

    fun onAppLocked() {
        mIsAppLocked.value = true
    }

    fun onAppUnlocked() {
        mIsAppLocked.value = false
    }

    fun onImageLongClicked(index: Int) {
        if(isAppLocked.value == true) return
        if(mSelectedImages.value!!.contains(index)) {
            mSelectedImages.value = mSelectedImages.value!! - index
        } else {
            mSelectedImages.value = mSelectedImages.value!! + index
        }
    }

    fun onDeleteSelectedImages() {
        val indexes = mSelectedImages.value!!
        mSelectedImages.value = emptyList()
        imagesModel.removeImagesAt(indexes)

    }

}