package pl.com.cartonrocket.photospresenter.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.github.chrisbanes.photoview.PhotoView
import pl.com.cartonrocket.photospresenter.R
import pl.com.cartonrocket.photospresenter.glide.OnResourceLoaded

class ImagesPagerAdapter(
    private val images: List<Uri>,
    private val onImageLoaded: (index: Int) -> Unit
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageView = LayoutInflater.from(container.context).inflate(R.layout.item_image_preview, container, false)
        container.addView(imageView)
        val listener = OnResourceLoaded { onImageLoaded.invoke(position) }
        Glide.with(imageView).load(images[position]).listener(listener).into(imageView as ImageView)
        return imageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, key: Any) {
        val view = key as View
        Glide.with(view).clear(view)
        container.removeView(view)
    }

    override fun startUpdate(container: ViewGroup) {
        super.startUpdate(container)
        (container.getChildAt(0) as? PhotoView)?.attacher?.scale = 1f
    }

    override fun isViewFromObject(view: View, key: Any): Boolean {
        return view == key
    }

    override fun getCount(): Int = images.size
}